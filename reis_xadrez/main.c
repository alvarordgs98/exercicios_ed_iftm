#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct info
{
    char nome_comp_1[30];
    char nome_comp_2[30];
    float res_comp_1;
    float res_comp_2;
} Info;

struct placar
{
    Info dados;
    struct placar *prox;
    struct placar *ant;
};

typedef struct placar Placar;

struct class
{
    char nom_comp[30];
    float pontuacao;
    int posicao;
    struct class *ant;
    struct class *prox;
};

typedef struct class Class;

struct listadPlacar
{
    Placar *ini;
    Placar *fim;
};

typedef struct listadPlacar ListadPlacar;

struct listadClass
{
    Class *ini;
    Class *fim;
};

typedef struct listadClass ListadClass;

Placar *criaNovoPlacar(Info dados);
Class *criaNovaClass();
ListadPlacar *criaListadPlacar();
ListadClass *criaListadClass();
void insere_fim_placar(Info dados, ListadPlacar *L);
void insere_fim_class(Class *novo, ListadClass *S);
void mostra_lista_placar(ListadPlacar *L);
void mostra_lista_class(ListadClass *S);
ListadPlacar *libera_listad(ListadPlacar *L);
void retira_duplicado(ListadPlacar *L);
void calc_pontuacao(ListadPlacar *L, ListadClass *S);
void ordenar_pontuacao(ListadClass *S);
void classificacao_competidores(ListadClass *S);

int main()
{
    ListadPlacar *L = criaListadPlacar();

    Info dados;
    int cont = 0;
    int n;
    scanf("%d", &n);

    while (scanf("%s %f %s %f", dados.nome_comp_1, &dados.res_comp_1, dados.nome_comp_2, &dados.res_comp_2) != EOF)
    {
        insere_fim_placar(dados, L);
    }

    ListadClass *S = criaListadClass();

    retira_duplicado(L);

    mostra_lista_class(S);

    calc_pontuacao(L, S);

    ordenar_pontuacao(S);

    classificacao_competidores(S);

    mostra_lista_class(S);
}

Placar *criaNovoPlacar(Info dados)
{
    Placar *novo = (Placar *)malloc(sizeof(Placar));
    novo->ant = NULL;
    novo->prox = NULL;
    strcpy(novo->dados.nome_comp_1, dados.nome_comp_1);
    novo->dados.res_comp_1 = dados.res_comp_1;
    strcpy(novo->dados.nome_comp_2, dados.nome_comp_2);
    novo->dados.res_comp_2 = dados.res_comp_2;

    return novo;
}

ListadPlacar *criaListadPlacar()
{
    ListadPlacar *L = (ListadPlacar *)malloc(sizeof(ListadPlacar));
    L->ini = NULL;
    L->fim = NULL;

    return L;
}

ListadClass *criaListadClass()
{
    ListadClass *L = (ListadClass *)malloc(sizeof(ListadClass));
    L->ini = NULL;
    L->fim = NULL;

    return L;
}

void insere_fim_placar(Info dados, ListadPlacar *L)
{
    Placar *novo = criaNovoPlacar(dados);
    if (L->fim == NULL)
    {
        L->ini = L->fim = novo;
    }
    else
    {
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

void insere_fim_class(Class *novo, ListadClass *S)
{

    if (S->fim == NULL)
    {
        S->ini = S->fim = novo;
    }
    else
    {
        novo->ant = S->fim;
        S->fim->prox = novo;
        S->fim = novo;
    }
}

void mostra_lista_placar(ListadPlacar *L)
{
    Placar *aux = L->ini;

    printf("\n\nMostrando a lista...\n");

    while (aux != NULL)
    {
        printf("\n%s %.1f %s %.1f", aux->dados.nome_comp_1, aux->dados.res_comp_1, aux->dados.nome_comp_2, aux->dados.res_comp_2);
        aux = aux->prox;
    }
}

void mostra_lista_class(ListadClass *S)
{
    Class *aux = S->ini;

    while (aux != NULL)
    {
        printf("\n%s %.1f %d", aux->nom_comp, aux->pontuacao, aux->posicao);
        aux = aux->prox;
    }
}

ListadPlacar *libera_listad(ListadPlacar *L)
{
    Placar *aux;

    while (L->ini != NULL)
    {
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }

    free(L);
    return NULL;
}

Class *criaNovaClass()
{
    Class *novo = (Class *)malloc(sizeof(Class));
    novo->ant = NULL;
    novo->prox = NULL;
    strcpy(novo->nom_comp, "");
    novo->pontuacao = 0;
    novo->posicao = 0;

    return novo;
}

void retira_duplicado(ListadPlacar *L)
{

    Placar *aux = L->ini, *proxElem, *aux2;

    while (aux != NULL)
    {
        aux2 = aux->prox;

        while (aux2 != NULL)
        {
            if (((strcmp(aux->dados.nome_comp_1, aux2->dados.nome_comp_1) == 0 && aux->dados.res_comp_1 == aux2->dados.res_comp_1) && (strcmp(aux->dados.nome_comp_2, aux2->dados.nome_comp_2) == 0 && aux->dados.res_comp_2 == aux2->dados.res_comp_2)) ||
                ((strcmp(aux->dados.nome_comp_2, aux2->dados.nome_comp_1) == 0 && aux->dados.res_comp_2 == aux2->dados.res_comp_1) && (strcmp(aux->dados.nome_comp_1, aux2->dados.nome_comp_2) == 0 && aux->dados.res_comp_1 == aux2->dados.res_comp_2)))
            {
                proxElem = aux2;
                aux2 = aux2->ant;

                if (proxElem->prox == NULL)
                {
                    proxElem->ant->prox = NULL;
                    L->fim = proxElem->ant;
                }
                else
                {
                    proxElem->ant->prox = proxElem->prox;
                    proxElem->prox->ant = proxElem->ant;
                }

                proxElem->prox = NULL;
                proxElem->ant = NULL;
                free(proxElem);
            }

            aux2 = aux2->prox;
        }

        aux = aux->prox;
    }
}

void calc_pontuacao(ListadPlacar *L, ListadClass *S)
{
    Placar *aux = L->ini;
    Class *aux_c;

    while (aux != NULL)
    {

        aux_c = S->ini;

        if (aux_c == NULL)
        {
            Class *novo1 = criaNovaClass();
            strcpy(novo1->nom_comp, aux->dados.nome_comp_1);
            novo1->pontuacao = aux->dados.res_comp_1;
            insere_fim_class(novo1, S);

            Class *novo2 = criaNovaClass();
            strcpy(novo2->nom_comp, aux->dados.nome_comp_2);
            novo2->pontuacao = aux->dados.res_comp_2;
            insere_fim_class(novo2, S);
        }
        else
        {

            while ((aux_c != NULL) && (strcmp(aux->dados.nome_comp_1, aux_c->nom_comp) != 0))
            {
                aux_c = aux_c->prox;
            }

            if (aux_c == NULL)
            {

                Class *novo1 = criaNovaClass();
                strcpy(novo1->nom_comp, aux->dados.nome_comp_1);
                novo1->pontuacao = aux->dados.res_comp_1;
                insere_fim_class(novo1, S);
            }
            else
            {
                aux_c->pontuacao += aux->dados.res_comp_1;
            }

            aux_c = S->ini;

            while ((aux_c != NULL) && (strcmp(aux->dados.nome_comp_2, aux_c->nom_comp) != 0))
            {
                aux_c = aux_c->prox;
            }

            if (aux_c == NULL)
            {

                Class *novo2 = criaNovaClass();
                strcpy(novo2->nom_comp, aux->dados.nome_comp_2);
                novo2->pontuacao = aux->dados.res_comp_2;
                insere_fim_class(novo2, S);
            }
            else
            {
                aux_c->pontuacao += aux->dados.res_comp_2;
            }
        }

        aux = aux->prox;
    }
}

void ordenar_pontuacao(ListadClass *S)
{
    Class *aux = S->ini, *aux2;
    char nome_comp[30];
    float pontuacao;

    while (aux != NULL)
    {
        aux2 = S->ini;
        while (aux2->prox != NULL && aux2->pontuacao >= aux2->prox->pontuacao)
        {
            aux2 = aux2->prox;
        }

        if (aux2->prox != NULL)
        {
            strcpy(nome_comp, aux2->prox->nom_comp);
            pontuacao = aux2->prox->pontuacao;

            strcpy(aux2->prox->nom_comp, aux2->nom_comp);
            aux2->prox->pontuacao = aux2->pontuacao;

            strcpy(aux2->nom_comp, nome_comp);
            aux2->pontuacao = pontuacao;
        }
        else
        {
            aux = NULL;
        }
    }
}

void classificacao_competidores(ListadClass *S)
{
    Class *aux = S->ini, *aux2, *aux3;
    char nomeAux[20];
    int cont_posicao = 1;
    aux3 = aux;

    while (aux != NULL)
    {
        if (aux == S->ini)
        {
            aux->posicao = cont_posicao;
        }
        else
        {
            if (aux->pontuacao == aux->ant->pontuacao)
            {
                aux->posicao = cont_posicao;

                aux2 = aux;

                while (aux2 != NULL && aux2->pontuacao == aux2->ant->pontuacao)
                {
                    if (strcmp(aux2->ant->nom_comp, aux2->nom_comp) > 0)
                    {
                        strcpy(nomeAux, aux2->ant->nom_comp);
                        strcpy(aux2->ant->nom_comp, aux2->nom_comp);
                        strcpy(aux2->nom_comp, nomeAux);

                        aux2 = aux3;
                    }

                    aux2 = aux2->prox;
                }
            }
            else
            {
                aux->posicao = ++cont_posicao;
                aux3 = aux;
            }
        }

        aux = aux->prox;
    }
}

/*
TO DO: melhorar a função de ordenaçao e criar funções para blocos de códigos repetidos
*/