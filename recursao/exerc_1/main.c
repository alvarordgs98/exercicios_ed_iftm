#include <stdlib.h>
#include <stdio.h>

struct no {
    struct no* prox;
    int info;
};

typedef struct no No;

No* cria_no(int info);
No* insere_fim(No* L, int info);
No* remove_fim(No* L);
void mostra_lista(No* L);
int conta_elemento(No* L, int count);

int main(){

    No* L = NULL;
    int count = 0;

    L = insere_fim(L, 10);
    L = insere_fim(L, 20);
    L = insere_fim(L, 30);
    L = insere_fim(L, 40);
    L = insere_fim(L, 50);

    mostra_lista(L);

    count = conta_elemento(L, count);

    printf("\nQuantidade de elementos: %d", count);

    return 0;
}

No* cria_no(int info) {
    No* novo = (No*) malloc(sizeof(No));

    novo->info = info;
    novo->prox = NULL;

    return novo;
}

No* insere_fim(No* L, int info) {
    No* novo = cria_no(info);

    if(L == NULL)
        L = novo;
    else {
        No* aux = L;
        while(aux->prox != NULL) {
            aux = aux->prox;
        }
        aux->prox = novo;
    }

    return L;
}

No* remove_fim(No* L) {
    No* aux = L, *aux2;
    int info;

    if(L != NULL) {
        while(aux->prox != NULL) {
            aux2 = aux;
            aux = aux->prox;
        }

        aux2->prox = NULL;
        info = aux->info;
        free(aux);
    }

    return L;
}

void mostra_lista(No* L) {
    No* aux = L;

    if(L == NULL)
        printf("\nLista vazia!");
    else {
        while(aux != NULL) {
            printf("%d ", aux->info);
            aux = aux->prox;
        }
    }
}

int conta_elemento(No* L, int count) {
    if(L == NULL)
        return count;
    else
        return conta_elemento(L->prox, count + 1);
}




