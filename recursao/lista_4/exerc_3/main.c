#include <stdio.h>
#include <stdlib.h>

int calculo_mdc(int x, int y) {
    if(x > y) {
        return calculo_mdc(x - y, y);
    } else if(x == y) {
        return x;
    } else {
        return calculo_mdc(y, x);
    }
}

int main() {
    int x, y, mdc;

    scanf("%d %d", &x, &y);

    mdc = calculo_mdc(x, y);

    printf("MDC: %d", mdc);
}