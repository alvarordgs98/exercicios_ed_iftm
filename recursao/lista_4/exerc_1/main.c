#include <stdio.h>
#include <stdlib.h>

void inverte_array(int *vet, int ini, int fim);

int main() {
    
    int qtdElementos, i;

    scanf("%d", &qtdElementos);

    int *vet = (int*) malloc(sizeof(int) * qtdElementos);

    for(i = 0; i < qtdElementos; i++) {
        scanf("%d", &vet[i]);
    }

    for(i = 0; i < qtdElementos; i++) {
        printf("%d ", vet[i]);
    }

    inverte_array(vet, 0, qtdElementos - 1);

    for(i = 0; i < qtdElementos; i++) {
        printf("%d ", vet[i]);
    }

    return 0;
}

void inverte_array(int *vet, int ini, int fim) {

    int aux = 0;

    if(ini < fim) {
        aux = vet[ini];
        vet[ini] = vet[fim];
        vet[fim] = aux;

        ini++;
        fim--;

        inverte_array(vet, ini, fim);
    }
}