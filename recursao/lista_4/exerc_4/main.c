#include <stdio.h>
#include <stdlib.h>

int calculo_mod(int x, int y) {
    if(x > y) {
        return calculo_mod(x - y, y);
    } else if(x < y) {
        return x;
    } else if(x == y) {
        return 0;
    }
}

int main() {
    int x, y, mod;

    scanf("%d %d", &x, &y);

    mod = calculo_mod(x, y);

    printf("MOD: %d", mod);
}