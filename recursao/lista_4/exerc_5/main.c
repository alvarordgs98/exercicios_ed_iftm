#include <stdio.h>
#include <stdlib.h>

/*
Escreva as funções recursivas que unem dois vetores, sem elementos repetidos, 
classificadas considerando que as duas listas não têm elementos em comum.
*/

int count_array(int i, int array[], int count) {
    
    if(array[i] != '\0') {
        count++;
        count_array(i + 1, array, count);
    } else {
        return count;
    }
}

int main() {
    int array1[10] = {1, 2, 3, 4, 5, 6};
    int array2[10], i = 0, count = 0;

    count = count_array(i, array1, count);

    printf("Qtd Array 1: %d", count);
}