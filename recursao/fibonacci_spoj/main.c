#include <stdlib.h>
#include <stdio.h>

int fibonacci(int num, int *count);

int main()
{

    int count, entrada, num, qtd_entradas, i;

    scanf("%d", &qtd_entradas);
    for (i = 0; i < qtd_entradas; i++)
    {   
        count = 0;

        scanf("%d", &entrada);
        num = fibonacci(entrada, &count);

        if(num == 1 || num == 0)
            count = 0;

        printf("\nfib(%d) = %d, fazendo %d chamadas", entrada, num, count-1);
    }

    return 0;
}

int fibonacci(int num, int *count)
{
    (*count)++;

    if (num == 0)
    {
        return 0;
    }
    if (num == 1)
    {
        return 1;
    }
    else
    {
        return fibonacci(num - 1, count) + fibonacci(num - 2, count);
    }

}
