#include <stdio.h>
#include <stdlib.h>

struct noabb
{
    int info;
    struct noabb *esq, *dir, *pai;
};
typedef struct noabb Noabb;

Noabb* cria_noabb(int info);
Noabb* insere(int info, Noabb* raiz);

void em_ordem(Noabb *raiz);
Noabb* insere_rec(int info, Noabb* raiz, Noabb* pai);

int main()
{
    Noabb *raiz = NULL;

    int qtdTotal, qtdNum, i, j, k, l;
    int *s1, *s2;

    scanf("%d", &qtdTotal);

    for(i = 0; i < qtdTotal; i++){

        scanf("%d", &qtdNum);

        s1 = (int*) malloc(sizeof(int) * qtdNum);

        for(j = 0; j < qtdNum; j++){
            scanf("%d;", &s1[j]);
            raiz = insere(s1[j], raiz);
        }

        s2 = (int*) malloc(sizeof(int) * qtdNum);

        for(j = 0; j < qtdNum; j++){
            scanf("%d;", &s2[j]);
        }

        pos_ordem(raiz);
        printf("\n");

        raiz = s1 = s2 = NULL;
    }

    return 0;
}

Noabb *cria_noabb(int info)
{
    Noabb *novo = malloc(sizeof(Noabb));
    novo->info = info;
    novo->dir = novo->esq = novo->pai = NULL;
    return novo;
}

Noabb *insere(int x, Noabb *raiz)
{
    Noabb *p = NULL, *q = raiz;
    Noabb *r = cria_noabb(x);

    if (raiz == NULL)
    {
        raiz = r;
    }
    else
    {
        while (q != NULL)
        {
            p = q;
            if (x < q->info)
                q = q->esq;
            else
                q = q->dir;
        }
        if (x < p->info)
            p->esq = r;
        else
            p->dir = r;
        r->pai = p;
    }
    return raiz;
}


void pos_ordem(Noabb *raiz)
{
    if (raiz != NULL)
    {
        pos_ordem(raiz->esq);
        pos_ordem(raiz->dir);
        printf("%d;", raiz->info);
    }
}
