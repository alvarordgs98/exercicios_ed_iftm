#include <stdio.h>
#include <stdlib.h>

struct noabb
{
    int info;
    struct noabb *esq, *dir, *pai, *prox;
};
typedef struct noabb Noabb;

Noabb *enfileira(Noabb *F, Noabb *raiz);
Noabb *desenfileira(Noabb **F);
Noabb *cria_noabb(int info);
Noabb *insere(int info, Noabb *raiz);
void em_ordem(Noabb *raiz);
void mostra_por_nivel(Noabb *filaPai, Noabb *filaFilha);

int main()
{
    Noabb *raiz = NULL;
    Noabb *filaPai = NULL, *filaFilha = NULL;

    int info;

    scanf("%d", &info);

    while (info != -1)
    {

        raiz = insere(info, raiz);

        scanf("%d", &info);
    }

    filaPai = enfileira(filaPai, raiz);

    mostra_por_nivel(filaPai, filaFilha);

    return 0;
}

Noabb *cria_noabb(int info)
{
    Noabb *novo = malloc(sizeof(Noabb));
    novo->info = info;
    novo->dir = novo->esq = novo->pai = novo->prox = NULL;
    return novo;
}

Noabb *insere(int x, Noabb *raiz)
{
    Noabb *p = NULL, *q = raiz;
    Noabb *r = cria_noabb(x);

    if (raiz == NULL)
    {
        raiz = r;
    }
    else
    {
        while (q != NULL)
        {
            p = q;
            if (x < q->info)
                q = q->esq;
            else
                q = q->dir;
        }
        if (x < p->info)
            p->esq = r;
        else
            p->dir = r;
        r->pai = p;
    }
    return raiz;
}

void mostra_por_nivel(Noabb *filaPai, Noabb *filaFilha)
{

    Noabb *elemPai, *elemFilha;

    if (filaPai != NULL)
    {
        while (filaPai != NULL)
        {
            elemPai = desenfileira(&filaPai);
            printf("%d ", elemPai->info);

            if (elemPai->esq != NULL)
                filaFilha = enfileira(filaFilha, elemPai->esq);

            if (elemPai->dir != NULL)
                filaFilha = enfileira(filaFilha, elemPai->dir);
        }

        filaPai = filaFilha;
        filaFilha = NULL;
        printf("\n");

        mostra_por_nivel(filaPai, filaFilha);
    }
}

/*void mostra_por_nivel(Noabb *filaPai, Noabb *filaFilha)
{

    Noabb *elemPai, *elemFilha;

    while(filaPai != NULL)
    {
        while (filaPai != NULL)
        {
            elemPai = desenfileira(&filaPai);
            printf("%d ", elemPai->info);

            if (elemPai->esq != NULL)
                filaFilha = enfileira(filaFilha, elemPai->esq);

            if (elemPai->dir != NULL)
                filaFilha = enfileira(filaFilha, elemPai->dir);

        }

        filaPai = filaFilha;
        filaFilha = NULL;
        printf("\n");
    }
}*/

void pos_ordem(Noabb *raiz)
{
    if (raiz != NULL)
    {
        pos_ordem(raiz->esq);
        pos_ordem(raiz->dir);
        printf("%d ", raiz->info);
    }
}

Noabb *enfileira(Noabb *F, Noabb *element)
{
    Noabb *aux;

    if (F == NULL)
    {
        F = element;
    }
    else
    {
        aux = F;

        while (aux->prox != NULL)
            aux = aux->prox;

        aux->prox = element;
    }

    return F;
}

Noabb *desenfileira(Noabb **F)
{
    Noabb *aux;

    if (*F != NULL)
    {
        aux = *F;

        *F = (*F)->prox;
    }

    return aux;
}
