#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct mes
{
    char nomeMes[12];
    int qtdDias;
    struct mes *prox;
};

typedef struct mes Mes;

struct dia
{
    char nomeDia[2];
    struct dia *prox;
};

typedef struct dia Dia;

// Funcoes referentes a lista de meses
Mes *cria_mes(char info[]);
Mes *insere_lista_mes(Mes *L, char info[]);
void mostra_lista_mes(Mes *L);
Mes *libera_lista_mes(Mes *L);

// Funcoes referentes a lista de dias da semana
Dia *cria_dia(char info[]);
Dia *insere_lista_dia(Dia *L, char info[]);
void mostra_lista_dia(Dia *L);
Dia *libera_lista_dia(Dia *L);

int verifica_ano_bissexto(int ano);
int verifica_dia_semana(int dia, int mes, int ano);
Mes *qtd_dias_em_cada_mes(Mes *Meses, int ano);

void mostra_calendario(Mes *Meses, Dia *Dias, int ano);

int main()
{
    Mes *Meses = NULL;
    Dia *Dias = NULL;
    int i;
    char mesesList[][11] = {"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"};
    char diaSemanaList[][2] = {"D", "S", "T", "Q", "Q", "S", "S"};

    for (int i = 0; i < 12; i++)
    {
        Meses = insere_lista_mes(Meses, mesesList[i]);
    }

    for (int i = 0; i < 7; i++)
    {
        Dias = insere_lista_dia(Dias, diaSemanaList[i]);
    }

    int ano = 0;
    printf("\nDigite o ano: ");
    scanf("%d", &ano);

    Meses = qtd_dias_em_cada_mes(Meses, ano);

    mostra_calendario(Meses, Dias, ano);
}

// Funcoes referentes a lista de meses
Mes *cria_mes(char info[])
{
    Mes *novo = (Mes *)malloc(sizeof(Mes));

    strcpy(novo->nomeMes, info);
    novo->qtdDias = 0;
    novo->prox = NULL;

    return novo;
}

Mes *insere_lista_mes(Mes *L, char info[])
{
    Mes *novo = cria_mes(info);

    Mes *aux = NULL;

    if (L == NULL)
    {
        novo->prox = novo;
        L = novo;
    }
    else
    {
        aux = L;

        while (aux->prox != L)
        {
            aux = aux->prox;
        }

        aux->prox = novo;
        novo->prox = L;
    }

    return L;
}

void mostra_lista_mes(Mes *L)
{

    if (L == NULL)
        printf("\n\nLista vazia!");
    else
    {
        Mes *aux2 = L->prox;

        printf("\n\n%s ", L->nomeMes);
        printf("\n%d ", L->qtdDias);

        while (aux2 != L)
        {
            printf("\n\n%s ", aux2->nomeMes);
            printf("\n%d ", aux2->qtdDias);

            aux2 = aux2->prox;
        }
    }
}

Mes *libera_lista_mes(Mes *L)
{
    Mes *aux = L, *aux2 = NULL;

    if (aux->prox != NULL)
        aux2 = aux->prox;

    while (aux2 != L)
    {
        aux = aux2;
        aux2 = aux2->prox;
        free(aux);
    }

    free(aux2);

    return NULL;
}

// Funcoes referentes a lista de dias da semana
Dia *cria_dia(char info[])
{
    Dia *novo = (Dia *)malloc(sizeof(Dia));

    strcpy(novo->nomeDia, info);
    novo->prox = NULL;

    return novo;
}

Dia *insere_lista_dia(Dia *L, char info[])
{
    Dia *novo = cria_dia(info);

    Dia *aux = NULL;

    if (L == NULL)
    {
        novo->prox = novo;
        L = novo;
    }
    else
    {
        aux = L;

        while (aux->prox != L)
        {
            aux = aux->prox;
        }

        aux->prox = novo;
        novo->prox = L;
    }

    return L;
}

void mostra_lista_dia(Dia *L)
{

    if (L == NULL)
        printf("\n\nLista vazia!");
    else
    {
        Dia *aux2 = L->prox;

        printf("\n\n%s ", L->nomeDia);

        while (aux2 != L)
        {
            printf("%s ", aux2->nomeDia);

            aux2 = aux2->prox;
        }
    }
}

Dia *libera_lista_dia(Dia *L)
{
    Dia *aux = L, *aux2 = NULL;

    if (aux->prox != NULL)
        aux2 = aux->prox;

    while (aux2 != L)
    {
        aux = aux2;
        aux2 = aux2->prox;
        free(aux);
    }

    free(aux2);

    return NULL;
}

int verifica_ano_bissexto(int ano)
{
    if ((ano % 4 == 0 && ano % 100 != 0) || (ano % 400 == 0))
        return 1;
    else
        return 0;
}

int verifica_dia_semana(int dia, int mes, int ano)
{
    int f = ano + dia + 3 * (mes - 1) - 1;

    if (mes < 3)
        ano--;
    else
        f -= (int)(0.4 * mes + 2.3);

    f += (int)(ano / 4) - (int)((ano / 100 + 1) * 0.75);

    return f %= 7;
}

Mes *qtd_dias_em_cada_mes(Mes *Meses, int ano)
{
    int i;
    Mes *aux = Meses;

    for (int i = 0; i < 12; i++)
    {
        switch (i)
        {
        case 0:
            aux->qtdDias = 31;
            break;
        case 1:
            if (verifica_ano_bissexto(ano))
                aux->qtdDias = 29;
            else
                aux->qtdDias = 28;
            break;
        case 2:
            aux->qtdDias = 31;
            break;
        case 3:
            aux->qtdDias = 30;
            break;
        case 4:
            aux->qtdDias = 31;
            break;
        case 5:
            aux->qtdDias = 30;
            break;
        case 6:
            aux->qtdDias = 31;
            break;
        case 7:
            aux->qtdDias = 31;
            break;
        case 8:
            aux->qtdDias = 30;
            break;
        case 9:
            aux->qtdDias = 31;
            break;
        case 10:
            aux->qtdDias = 30;
            break;
        case 11:
            aux->qtdDias = 31;
            break;
        }

        aux = aux->prox;
    }

    return Meses;
}

void mostra_calendario(Mes *Meses, Dia *Dias, int ano)
{
    Mes *aux = Meses;
    Dia *aux2 = Dias;
    int i, valorMes, contDiasMes, contDiasSem, diaSemana;
    valorMes = 1;

    do
    {
        printf("\n\n%s\n", aux->nomeMes);

        do
        {
            printf("%s  ", aux2->nomeDia);
            aux2 = aux2->prox;

        } while (aux2 != Dias);
        printf("\n");

        contDiasSem = 0;
        for (i = 1; i <= aux->qtdDias; i++)
        {
            diaSemana = verifica_dia_semana(i, valorMes, ano);

            if (i == 1)
            {
                switch (diaSemana)
                {
                case 0:
                    printf("%d  ", i);
                    break;
                case 1:
                    printf("   %d  ", i);
                    contDiasSem = 1;
                    break;
                case 2:
                    printf("      %d  ", i);
                    contDiasSem = 2;
                    break;
                case 3:
                    printf("         %d  ", i);
                    contDiasSem = 3;
                    break;
                case 4:
                    printf("            %d  ", i);
                    contDiasSem = 4;
                    break;
                case 5:
                    printf("               %d  ", i);
                    contDiasSem = 5;
                    break;
                case 6:
                    printf("                  %d  ", i);
                    contDiasSem = 6;
                    break;
                }
            } else
            {
                if(i > 9)
                    printf("%d ", i);
                else
                    printf("%d  ", i);
            }

            if (contDiasSem == 6)
            {
                contDiasSem = 0;
                printf("\n");
            }
            else
            {
                contDiasSem++;
            }
        }

        aux = aux->prox;
        valorMes++;

    } while (aux != Meses);
}