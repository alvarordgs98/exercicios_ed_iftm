#include <stdio.h>
#include <stdlib.h>

struct nod
{
    int info;
    struct nod *ant;
    struct nod *prox;
};
typedef struct nod Nod;

struct listad
{
        Nod *ini;
        Nod *fim;
};
typedef struct listad Listad;

typedef Listad Fila;

Fila *cria_fila();
Fila *enqueue(Fila *f, int elemento); // insere
int dequeue(Fila *f);                 // remove
int fila_esta_vazia(Fila *f);
int peek(Fila *f); // consulta inicio
void libera_fila(Fila *p);
void mostra_fila(Fila *f);
void fila_final(Fila* pessoasEntraram, Fila* pessoasSairam);

Listad* cria_listad();
Nod* cria_nod(int valor);
Listad* insere_inicio_listad(Listad *L, int valor);
Listad* insere_fim_listad(Listad *L, int valor);
int remove_fim_listad(Listad *L);
int remove_inicio_listad(Listad *L);
Listad* libera_listad(Listad *L);

/* Funçao principal */
/***********************************************/

int main()
{
    int qtdPessoasInicFila, qtdPessoasSairamFila, i, idPessoa;

    Fila *pessoasEntraram = cria_fila();
    Fila *pessoasSairam = cria_fila();
    Fila *pessoasRestaram = cria_fila();

    scanf("%d", &qtdPessoasInicFila);
    for (i = 0; i < qtdPessoasInicFila; i++)
    {
        scanf("%d", &idPessoa);
        pessoasEntraram = enqueue(pessoasEntraram, idPessoa);
    }

    //mostra_fila(pessoasEntraram);

    scanf("%d", &qtdPessoasSairamFila);
    for (i = 0; i < qtdPessoasSairamFila; i++)
    {
        scanf("%d", &idPessoa);
        pessoasSairam = enqueue(pessoasSairam, idPessoa);
    }

    //mostra_fila(pessoasSairam);

    fila_final(pessoasEntraram, pessoasSairam);

    mostra_fila(pessoasEntraram);

    return 0;
}

/* Funçoes */
/***********************************************/
Fila *cria_fila()
{
    return cria_listad();
}

Fila *enqueue(Fila *f, int elemento)
{
    return insere_fim_listad(f, elemento);
}
int dequeue(Fila *f)
{
    return remove_inicio_listad(f);
}

int fila_esta_vazia(Fila *f)
{
    if (f == NULL || f->ini == NULL)
        return 1;
    else
        return 0;
}

int peek(Fila *f)
{
    if (fila_esta_vazia(f))
        return -1;
    else
        return f->ini->info;
}
void libera_fila(Fila *f)
{

    libera_listad(f);
}

void mostra_fila(Fila *f)
{

    if (f->ini == NULL)
    {
        printf("\n Fila vazia!");
    }
    else
    {   
        Nod *aux = f->ini;

        while (aux != NULL)
        {
            printf("%d ", aux->info);
            aux = aux->prox;
        }
    }
}

void fila_final(Fila *pessoasEntraram, Fila *pessoasSairam)
{
    Fila *aux = pessoasEntraram;
    Nod *aux2 = pessoasSairam->ini;
    Nod *aux3;
    Fila *f = cria_fila();
    int valor;

    while (aux2 != NULL)
    {
        while (aux->ini != NULL && aux->ini->info != aux2->info)
        {
            valor = dequeue(aux);
            f = enqueue(f, valor);

        }

        if (aux->ini != NULL)
        {
            int valorSaida = dequeue(aux);

            while (aux->ini != NULL && aux->ini->info != aux2->info)
            {
                valor = dequeue(aux);
                f = enqueue(f, valor);

            }

            aux->ini = f->ini;
            aux->fim = f->fim;
            f->ini = NULL;
        }

        aux = pessoasEntraram;
        aux2 = aux2->prox;
    }
}

Listad* cria_listad()
{
    Listad* novalista;
    novalista = (Listad *)malloc(sizeof(Listad));
    novalista->ini = novalista->fim = NULL;
    return novalista;
}

Nod* cria_nod(int valor)
{
    Nod* novo = (Nod*)malloc(sizeof(Nod));
    novo->ant = novo->prox = NULL;
    novo->info = valor;
    return novo;
}


Listad* insere_inicio_listad(Listad *L, int valor)
{
    Nod *novo= cria_nod(valor);

    if (L == NULL)
    {
        L = cria_listad();
        L->ini = L->fim = novo;

    }
    else
    {
        if (L->ini == NULL)
            L->ini = L->fim = novo;
        else
        {
            novo->prox = L->ini;
            L->ini->ant = novo;
            L->ini = novo;
        }
    }
    return L;

}



Listad* insere_fim_listad(Listad* L, int valor)
{
    Nod *novo = cria_nod(valor);

    if (L == NULL)
    {
        L = cria_listad();
        L->ini = L->fim = novo;
    }
    else
    {


        if(L->ini == NULL)
        {
            L->ini = L->fim = novo;
        }
        else
        {
            novo->ant = L->fim;
            L->fim->prox = novo;
            L->fim = novo;
        }
    }
    return L;
}

int remove_inicio_listad(Listad *L)
{
    Nod* aux;
    int resposta = -1;//quando nao tem nada pra remover
    if (L!=NULL)
        if (L->ini != NULL)
        {
            aux = L->ini;

            if (L->ini != L->fim)
            {
                L->ini->prox->ant = NULL;
                L->ini = L->ini->prox;
            }
            else
                L->ini = L->fim = NULL;

            resposta = aux->info;
            free(aux);
        }
    
    return resposta;
}

int remove_fim_listad(Listad *L)
{
    Nod* aux;
    int resposta = -1;

    if(L != NULL && L->fim != NULL)
    {
        aux = L->fim;
        if(L->ini != L->fim)//mais de um elemento na lista
        {
            L->fim->ant->prox = NULL;
            L->fim = L->fim->ant;
        }
        else//s� tem um elemento na lista
            L->ini = L->fim = NULL;

        resposta = aux->info;
        free(aux);
    }
    return resposta;
}

Listad* libera_listad(Listad *L)
{
    Nod *aux = L->ini;

    while (aux != NULL)
    {
        L->ini = L->ini->prox;
        free(aux);
        aux = L->ini;
    }

    free(L);
    return NULL;

}
