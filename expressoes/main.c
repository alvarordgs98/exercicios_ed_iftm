#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define tamanho 100001

struct no
{
    char valor;
    struct no *prox;
};

typedef struct no No;

No *cria_no(char info);
No *empilha(No *P, char info);
char desempilha(No **P);
int pilha_vazia(No *P);
void mostra_pilha(No *P);
int verifica_caractere(char simbol);
void liberar_pilha(No *P);

int main()
{
    No *P = NULL;
    int tam, i, j, tipoCaract, vlAbertura;
    char info[tamanho];
    char caract = ' ', resp = ' ';

    scanf("%d", &tam);
    getchar();

    for (i = 0; i < tam; i++)
    {
        fflush(stdin);
        gets(info);

        j = 0;
        if(((int) strlen(info)) % 2 != 0)
            j = -1;

        while (j < ((int)strlen(info)) && j != -1)
        {
            caract = info[j];

            tipoCaract = verifica_caractere(caract);

            if ((tipoCaract == 10 || tipoCaract == 20 || tipoCaract == 30))
            {
                if (tipoCaract != 0)
                {
                    if (j == 0)
                    {
                        j = -1;
                    }
                    else
                    {

                        if (pilha_vazia(P) == 0)
                        {
                            vlAbertura = verifica_caractere(desempilha(&P));

                            if (tipoCaract == vlAbertura * 10)
                            {
                                j++;
                            }
                            else
                            {
                                j = -1;
                            }
                        }
                    }
                }
                else
                {
                    j = -1;
                }
            }
            else
            {
                P = empilha(P, caract);
                j++;
            }
        }

        if (pilha_vazia(P) == 1 && j != -1)
        {
            resp = 'S';
        }
        else
        {
            resp = 'N';
        }

        printf("%c\n", resp);

        liberar_pilha(P);
        P = NULL;
    }

    return 0;
}

No *cria_no(char info)
{
    No *novo = (No *)malloc(sizeof(No));

    novo->valor = info;
    novo->prox = NULL;

    return novo;
}

No *empilha(No *P, char info)
{
    No *novo = cria_no(info);

    if (P == NULL)
    {
        P = novo;
    }
    else
    {
        novo->prox = P;
        P = novo;
    }

    return P;
}

char desempilha(No **P)
{
    No *aux;
    char caract = ' ';

    if (*P != NULL)
    {
        aux = *P;
        *P = (*P)->prox;
        caract = aux->valor;
        free(aux);
    }

    return caract;
}

int pilha_vazia(No *P)
{
    return (P == NULL);
}

void mostra_pilha(No *P)
{
    No *aux = P;

    while (aux != NULL)
    {
        printf("%c\n", aux->valor);

        aux = aux->prox;
    }
}

int verifica_caractere(char simbol)
{

    int value;

    switch (simbol)
    {
    case '{':
        value = 1;
        break;
    case '[':
        value = 2;
        break;
    case '(':
        value = 3;
        break;
    case '}':
        value = 10;
        break;
    case ']':
        value = 20;
        break;
    case ')':
        value = 30;
        break;
    default:
        value = 0;
        break;
    }

    return value;
}

void liberar_pilha(No *P)
{
    No *aux;

    while (P != NULL)
    {
        aux = P;
        P = P->prox;
        free(aux);
    }
}