#include <stdio.h>
#include <stdlib.h>

//cabeçalho das funções

struct no {
    int valor;
    struct no* prox;
};

typedef struct no No;

No* criarNovo(int valor);
No* inserirNoFim(No* L, int valor);
void mostrar(No* L);
No* interseccao(No* A, No* B);
No* juncao(No* A, No* B);

//função main

int main() {

    int n, m, i, valor = 0;
    No *A = NULL, *B = NULL, *inter = NULL, *uniao = NULL; 

    scanf("%d %d", &n, &m);

    for(i = 0; i < n; i++) {
        scanf("%d", &valor);
        A = inserirNoFim(A, valor);
    }

    for(i = 0; i < m; i++) {
        scanf("%d", &valor);
        B = inserirNoFim(B, valor);
    }

    inter = interseccao(A, B);

    mostrar(inter);

    uniao = juncao(A, B);

    mostrar(uniao);

    return 0;
}

//corpo das funções

No* criarNovo(int valor){
    No* novo = (No*) malloc(sizeof(No));
    novo->valor = valor;
    novo->prox = NULL;
    
    return novo;
}

No* inserirNoFim(No* L, int valor){

    No* novo = criarNovo(valor);
    
    if(L == NULL) {
        L = novo;
    } else {
        No * aux = L;

        while(aux->prox != NULL){
            aux = aux->prox;
        }

        aux->prox = novo;
    }

    return L;
}


void mostrar(No* L){
    No* aux = L;

    while(aux != NULL){
        printf("%d ", aux->valor);
        aux = aux->prox;
    }
    printf("\n");
}

No* interseccao(No* A, No* B) {
    No* inter = NULL;
    No* auxa = A, *auxb = NULL;

    while(auxa != NULL) {
        auxb = B;

        while(auxb != NULL && auxb->valor <= auxa->valor) {
            if(auxb->valor == auxa->valor)
                inter = inserirNoFim(inter, auxa->valor);

            auxb = auxb->prox;
        }

        auxa = auxa->prox;
    }

    return inter;
}

No* juncao(No* A, No* B) {
    No* uniao = NULL, *auxa = A, *auxb = B;

    while(auxa != NULL && auxb != NULL) {
        if(auxa->valor < auxb->valor) {
            uniao = inserirNoFim(uniao, auxa->valor);
            auxa = auxa->prox;
        } else if(auxa->valor > auxb->valor) {
            uniao = inserirNoFim(uniao, auxb->valor);
            auxb = auxb->prox;
        } else {
            uniao = inserirNoFim(uniao, auxa->valor);
            auxa = auxa->prox;
            auxb = auxb->prox;
        }
    }

    while(auxa != NULL) {
        uniao = inserirNoFim(uniao, auxa->valor);
        auxa = auxa->prox;
    }

    while(auxb != NULL) {
        uniao = inserirNoFim(uniao, auxb->valor);
        auxb = auxb->prox;
    }

    return uniao;
}