#include <stdio.h>
#include <stdlib.h>
struct nod
{
    int info;
    struct nod *prox;
    struct nod *ant;
};
typedef struct nod Nod;
struct listad
{
    Nod *ini, *fim;
};
typedef struct listad Listad;
Listad* cria_listad();
Nod* cria_nod(int info);
void insere_inicio_listad(int info, Listad *L);
void insere_fim_listad(int info, Listad *L);
void mostra_listad(Listad *L);
Listad* libera_listad(Listad *L);
int escadinha(Listad* L) ;

int main()
{
    Listad *L = cria_listad();
    int qtdNum = 0, i, valor = 0;

    scanf("%d", &qtdNum);

    for(i = 0; i < qtdNum; i++)
    {
        scanf("%d", &valor);
        insere_fim_listad(valor, L);
    }

    /*mostra_listad(L);*/

    int qtdEscadinha = escadinha(L);

    printf("%d", qtdEscadinha);

    L = libera_listad(L);

    return 0;
}

Listad* cria_listad()
{
    Listad *L = (Listad*)malloc(sizeof(Listad));
    L->ini = L->fim = NULL;
    return L;
}

Nod* cria_nod(int info)
{
    Nod *novo= (Nod*)malloc(sizeof(Nod));
    novo->info = info;
    novo->ant = novo->prox = NULL;
    return novo;
}

void insere_inicio_listad(int info, Listad *L)
{
    Nod *novo = cria_nod(info);

    if (L->ini == NULL)
    {
        L->ini = L->fim = novo;
    }
    else
    {
        novo->prox = L->ini;
        L->ini->ant = novo;
        L->ini = novo;
    }
}

void insere_fim_listad(int info, Listad *L)
{
    Nod* novo = cria_nod(info);

    if (L->ini==NULL)
    {
        L->ini = L->fim = novo;
    }
    else
    {
        novo->ant = L->fim;
        L->fim->prox = novo;
        L->fim = novo;
    }
}

void mostra_listad(Listad *L)
{
    Nod *aux = L->ini;

    while (aux != NULL)
    {
        printf("%d ", aux->info);
        aux = aux->prox;
    }
}
Listad* libera_listad(Listad *L)
{
    Nod* aux;

    while(L->ini != NULL)
    {
        aux = L->ini;
        L->ini = L->ini->prox;
        free(aux);
    }
    free(L);
    return NULL;
}

int escadinha(Listad* L) 
{
    Nod* aux = L->ini;
    int qtdEscadinha = 0, dif1 = 0, dif2 = 0;

    if(aux->prox == NULL )
    {
        qtdEscadinha++;
    } 
    else if(aux->prox->prox == NULL){
        qtdEscadinha++;
    }
    else{
        while(aux->prox != NULL)
        {
            dif1 = aux->prox->info - aux->info;
            aux = aux->prox;

            while(aux->prox != NULL && dif1 == (aux->prox->info - aux->info))
            {
                aux = aux->prox;
            } 
            
            qtdEscadinha++;
        }
    }

    return qtdEscadinha;
}



