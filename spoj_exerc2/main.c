/*
Estrutura de dados - Atividade Avaliativa 1
Alunos: Álvaro Silva Rodrigues
        Arthur Santana
        Alberto Domingos
Turma: ED 2023/01
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pares
{
    char b1;
    char b2;
    struct pares *prox;
};

typedef struct pares Pares;

struct contagem
{
    char gene;
    int qtdGene;
    struct contagem *prox;
};

typedef struct contagem Contagem;

Pares *inserirNoFim(Pares *L);
Pares *criarNovo();
Contagem *contagemGenes(Pares *L, Contagem *cont, int i);
Contagem *inserirContagem(Contagem *cont, Contagem *nvCont);
Contagem* ordenarContagem(Contagem* cont);
void mostraContagem(Contagem *cont);
// void mostraEntrada(Pares* L);

// funcao main

int main()
{

    int qtdPares = 0, i;
    Pares *L = NULL;
    Contagem *cont = NULL;

    scanf("%d", &qtdPares);

    while (qtdPares != 0) {

        for (i = 0; i < qtdPares; i++)
        {
            L = inserirNoFim(L);
        }

        // mostraEntrada(L);

        for (i = 0; i < 4; i++)
        {
            cont = contagemGenes(L, cont, i);
        }

        cont = ordenarContagem(cont);

        mostraContagem(cont);

        L = NULL;
        cont = NULL;

        scanf("%d", &qtdPares);
    }

    L = NULL;
    cont = NULL;

    return 0;
}

// corpo das funcoes

Pares *criarNovo()
{
    Pares *novo = (Pares *)malloc(sizeof(Pares));

    getchar();
    scanf("%c %c", &novo->b1, &novo->b2);
    
    novo->prox = NULL;

    return novo;
}

// inserir no fim da lista de pares de genes
Pares *inserirNoFim(Pares *L)
{

    Pares *novo = criarNovo();

    if (L == NULL)
    {
        L = novo;
    }
    else
    {
        Pares *aux = L;

        while (aux->prox != NULL)
        {
            aux = aux->prox;
        }

        aux->prox = novo;
    }

    return L;
}

// contar a quantidade de cada gene e inserir em uma lista
Contagem *contagemGenes(Pares *L, Contagem *cont, int i)
{

    Pares *aux = NULL;
    Contagem *aux2 = NULL;

    Contagem *nvCont = (Contagem *)malloc(sizeof(Contagem));
    nvCont->qtdGene = 0;
    nvCont->prox = NULL;

    // contagem por gene
    switch (i)
    {
    case 0:
        nvCont->gene = 'A';
        break;
    case 1:
        nvCont->gene = 'T';
        break;
    case 2:
        nvCont->gene = 'C';
        break;
    case 3:
        nvCont->gene = 'G';
        break;
    }

    aux = L;
    while (aux != NULL)
    {

        if (aux->b1 == nvCont->gene || aux->b2 == nvCont->gene)
        {
            if (aux->b1 == nvCont->gene && aux->b2 == nvCont->gene)
            {
                nvCont->qtdGene += 2;
            }
            else if (aux->b1 == nvCont->gene)
            {
                nvCont->qtdGene++;
            }
            else if (aux->b2 == nvCont->gene)
            {
                nvCont->qtdGene ++;
            }
        }

        aux = aux->prox;
    }

    cont = inserirContagem(cont, nvCont);

    return cont;
}

// insere em uma lista a quantidade de genes por gene
Contagem *inserirContagem(Contagem *cont, Contagem *nvCont)
{
    Contagem *aux = NULL;

    // colocar na lista
    if (cont == NULL)
    {
        cont = nvCont;
    }
    else
    {

        aux = cont;
        while (aux->prox != NULL)
        {
            aux = aux->prox;
        }

        aux->prox = nvCont;
    }

    return cont;
}

// ordena a lista pela quantidade ou pelas letras do alfabeto
Contagem* ordenarContagem(Contagem* cont) {
    Contagem *aux = cont, *aux2 = cont;
    int guardaQtd = 0;
    char guardaChar = ' ';

    while(aux != NULL) {

        aux2 = aux;
        while(aux2 != NULL) {

            if(aux->qtdGene > aux2->qtdGene) {

                //Troca a qtd de genes encontrados
                guardaQtd = aux2->qtdGene;
                aux2->qtdGene = aux->qtdGene;
                aux->qtdGene = guardaQtd;

                //Troca o caractere que representa o gene
                guardaChar = aux2->gene;
                aux2->gene = aux->gene;
                aux->gene = guardaChar;

            } else if(aux->qtdGene == aux2->qtdGene && aux->gene != aux2->gene) {
                if(aux->gene > aux2->gene) {
                    //Troca o caractere que representa o gene
                    guardaChar = aux2->gene;
                    aux2->gene = aux->gene;
                    aux->gene = guardaChar;
                }
            }

            aux2 = aux2->prox;
        }

        aux = aux->prox;
    }

    return cont;
}

// mostra a lista da contagem de gene
void mostraContagem(Contagem *cont)
{
    Contagem *aux = cont;

    while (aux != NULL)
    {
        printf("%c %d\n", aux->gene, aux->qtdGene);
        aux = aux->prox;
    }
    printf("\n");
}

// void mostraEntrada(Pares* L) {
//     Pares* aux = L;

//     while(aux != NULL) {
//         printf("%c %c\n", aux->b1, aux->b2);
//         aux = aux->prox;
//     }
// }